<!--
    SPDX-License-Identifier: CC0-1.0
    SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>
-->

# The Crashinator

Simple tool to crash in a number of ways. Useful for testing drkonqi/coredumpd.

![Screenshot](https://invent.kde.org/sitter/crashinator/-/raw/master/screenshot.png)
