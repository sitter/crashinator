// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

#include <csignal>
#include <sys/types.h>
#include <fcntl.h>

#include <QCommandLineParser>
#include <QDebug>
#include <QGuiApplication>
#include <QMutex>
#include <QQmlApplicationEngine>
#include <QVulkanFunctions>
#include <QVulkanInstance>
#include <QtConcurrent/QtConcurrent>

#include <KAboutData>
#include <KCrash>

using namespace std::chrono_literals;
using namespace Qt::StringLiterals;

class ExceptingThread : public QThread
{
public:
    void run() override
    {
        throw std::runtime_error("exception in thread");
    }
};

class Helper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool autoCrashFromQml MEMBER m_autoCrashFromQml)
public:
    using QObject::QObject;

    bool m_autoCrashFromQml = false;

public Q_SLOTS:
    // NOTE: we invoke method so the crash happens outside the QML call chain. This allows us to create both traces
    // with or without QML involvement.

    void queueHang()
    {
        QMetaObject::invokeMethod(
            this,
            [] {
                while (true) { }
            },
            Qt::QueuedConnection);
    }

    void queueTemporaryHang()
    {
        QMetaObject::invokeMethod(
            this,
            [] {
                std::this_thread::sleep_for(32s);
            },
            Qt::QueuedConnection);
    }

    void queueCrash()
    {
        QMetaObject::invokeMethod(
            this, [this] { crash(); }, Qt::QueuedConnection);
    }

    void queueABRT()
    {
        kill(getpid(), SIGABRT);
    }

    void queueSEGV()
    {
        kill(getpid(), SIGSEGV);
    }

    void crash()
    {
        deadObject()->metaObject();
    }

    QObject *deadObject()
    {
        return (QObject*)0xDEAD;
    }

    void crashFromQmlMaybe()
    {
        if (m_autoCrashFromQml) {
            crash();
        }
    }

    void deadlock()
    {
        QMetaObject::invokeMethod(this, &Helper::deadlockInternal, Qt::QueuedConnection);
    }

    void deadlockInternal()
    {
        QMutex mutex;
        QMutex sync; // ensure we only lock the mutex once the runnable has locked it
        QWaitCondition condition;
        sync.lock();
        std::ignore = QtConcurrent::run([&mutex, &sync, &condition] {
            sync.lock();
            mutex.lock();
            condition.wakeAll();
            sync.unlock();
        });
        condition.wait(&sync);
        mutex.lock();
    }

    void exception()
    {
        throw std::runtime_error("exception");
    }

    void exceptionInThread()
    {
        ExceptingThread t;
        t.start();
        t.wait();
    }

    void exceptionInFuture()
    {
        QtConcurrent::run([] {
            throw std::runtime_error("exception in thread");
        }).waitForFinished();
    }

    void threadedCrash()
    {
        // Threaded crashing is a bit of a challenge to negotiate timing-wise. wakecondition should help.
        QMutex mutex;
        mutex.lock();
        QWaitCondition condition;
        std::ignore = QtConcurrent::run([&mutex, &condition] {
            mutex.lock();
            condition.wakeAll();
            mutex.unlock();
            KCrash::defaultCrashHandler(SIGSEGV);
        });
        condition.wait(&mutex);
        KCrash::defaultCrashHandler(SIGSEGV);
    }

    void stackExhaustionCrash()
    {
        stackExhaustionCrash();
    }

    void exhaustFds()
    {
        while (true) {
            int fd = open("/dev/null", O_RDONLY);
            if (fd == -1) {
                qFatal("Failed to open /dev/null");
            }
        }
    }

    void incompatibleQt()
    {
        qFatal("Cannot mix incompatible Qt library (6.7.2) with this library (6.9.0)");
    }
};

namespace
{
[[nodiscard]] QVariantHash gpuContext()
{
    QVulkanInstance instance;
    if (!instance.create()) {
        qWarning() << "Failed to create vulkan instance";
        return {};
    }
    auto functions = instance.functions();

    uint32_t count = 0;
    functions->vkEnumeratePhysicalDevices(instance.vkInstance(), &count, nullptr);
    if (count == 0) {
        qWarning("No vulkan devices");
        return {};
    }

    QVarLengthArray<VkPhysicalDevice, 4> devices(count);
    VkResult error = functions->vkEnumeratePhysicalDevices(instance.vkInstance(), &count, devices.data());
    if (error != VK_SUCCESS || count == 0) {
        qWarning("Failed to enumerate vulkan devices: %d", error);
        return {};
    }

    for (const auto &device : devices) {
        VkPhysicalDeviceProperties properties;
        functions->vkGetPhysicalDeviceProperties(device, &properties);
        return {
            {u"name"_s, QString::fromUtf8(properties.deviceName)}, //
            {u"version"_s,
             u"%1.%2.%3"_s.arg(VK_VERSION_MAJOR(properties.driverVersion))
                 .arg(VK_VERSION_MINOR(properties.driverVersion))
                 .arg(VK_VERSION_PATCH(properties.driverVersion))}, //
            {u"id"_s, QString::number(properties.deviceID, 16)}, //
            {u"vendor_id"_s, QString::number(properties.vendorID, 16)},
            {u"api_type"_s, u"Vulkan"_s},
        };
    }

    return {};
}
} // namespace

int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);
    KCrash::initialize();
    KCrash::setErrorTags({
        {u"fancyTag"_s, u"fancyValue"_s}, //
        {u"sweetTag"_s, u"sweetValue"_s}, //
    });
    KCrash::setErrorExtraData({
        {u"daftExtra"_s, u"DAFT I SAY"_s}, //
    });
    KCrash::setErrorMessage(u"Crashinator has crashed"_s);
    KCrash::setGPUData(gpuContext());

    QCommandLineParser parser;
    parser.addHelpOption();
    QCommandLineOption crashOption(QStringList{u"crash"_s}, "Crash"_L1);
    parser.addOption(crashOption);
    QCommandLineOption crashQmlOption(QStringList{u"crash-qml"_s}, "Crash from QML"_L1);
    parser.addOption(crashQmlOption);
    QCommandLineOption hangOption(QStringList{u"hang"_s}, "Hang"_L1);
    parser.addOption(hangOption);

    KAboutData data(u"crashinator"_s, u"Crashinator"_s, u"1.0"_s, u"A crashing application"_s, KAboutLicense::GPL);
    KAboutData::setApplicationData(data);
    data.setupCommandLine(&parser);

    parser.process(app);

    Helper helper;

    if (parser.isSet(crashQmlOption)) {
        helper.m_autoCrashFromQml = true;
    } else if (parser.isSet(crashOption)) {
        helper.queueCrash();
    } else if (parser.isSet(hangOption)) {
        helper.queueHang();
    }

    QQmlApplicationEngine engine;
    qmlRegisterSingletonInstance("org.kde.crashinator", 1, 0, "Helper", &helper);

    const QUrl mainUrl("qrc:/ui/main.qml"_L1);
    QObject::connect(&engine,
                     &QQmlApplicationEngine::objectCreated,

                     [mainUrl](QObject *obj, const QUrl &objUrl) {
                         if (!obj && mainUrl == objUrl) {
                             qWarning() << "Failed to load QML dialog";
                             abort();
                         }
                     });
    engine.load(mainUrl);

    return app.exec();
}

#include "main.moc"
