// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.19 as Kirigami

import org.kde.crashinator 1.0

Kirigami.ApplicationWindow {
    minimumWidth: Kirigami.Settings.isMobile ? 0 : Kirigami.Units.gridUnit * 12
    minimumHeight: Kirigami.Settings.isMobile ? 0 : Kirigami.Units.gridUnit * 32

    width: minimumWidth
    height: minimumHeight

    QQC2.ScrollView {
        anchors.fill: parent
        anchors.margins: Kirigami.Units.smallSpacing

        ColumnLayout {
            QQC2.Button {
                action: Kirigami.Action {
                    text: 'Crash Application'
                    onTriggered: Helper.queueCrash()
                }
            }

            QQC2.Button {
                action: Kirigami.Action {
                    text: 'Crash from QML'
                    onTriggered: Helper.crash()
                }
            }

            QQC2.Button {
                action: Kirigami.Action {
                    text: 'Crash in 2 Threads'
                    onTriggered: Helper.threadedCrash()
                }
            }

            QQC2.Button {
                action: Kirigami.Action {
                    text: 'Hang Application'
                    onTriggered: Helper.queueHang()
                }
            }

            QQC2.Button {
                action: Kirigami.Action {
                    text: 'Mutex Deadlock'
                    onTriggered: Helper.deadlock()
                }
            }

            QQC2.Button {
                action: Kirigami.Action {
                    text: 'Exception'
                    onTriggered: Helper.exception()
                }
            }

            QQC2.Button {
                action: Kirigami.Action {
                    text: 'Exception in Thread'
                    onTriggered: Helper.exceptionInThread()
                }
            }

            QQC2.Button {
                action: Kirigami.Action {
                    text: 'Exception in QFuture'
                    onTriggered: Helper.exceptionInFuture()
                }
            }

            QQC2.Button {
                action: Kirigami.Action {
                    text: 'Stack Exhaustion'
                    onTriggered: Helper.stackExhaustionCrash()
                }
            }

            QQC2.Button {
                action: Kirigami.Action {
                    text: 'ABRT Application'
                    onTriggered: Helper.queueABRT()
                }
            }

            QQC2.Button {
                action: Kirigami.Action {
                    text: 'SEGV Application'
                    onTriggered: Helper.queueSEGV()
                }
            }

            QQC2.Button {
                action: Kirigami.Action {
                    text: 'Hang Application (32s)'
                    onTriggered: Helper.queueTemporaryHang()
                }
            }

            QQC2.Button {
                action: Kirigami.Action {
                    text: 'Exhaust FDs'
                    onTriggered: Helper.exhaustFds()
                }
            }

            QQC2.Button {
                action: Kirigami.Action {
                    text: 'Incompatible Qt'
                    onTriggered: Helper.incompatibleQt()
                }
            }
        }
    }

    Component.onCompleted: Helper.crashFromQmlMaybe()
}

